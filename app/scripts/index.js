// import "./markup-menu";
import main from "./components/main";
import svg4everybody from "svg4everybody";
import scroll from "./components/scroll";
import slider from "./components/slider";
import map from "./components/map";
import Tabs from "./components/tabs";

document.addEventListener("DOMContentLoaded", () => {
  main();
  svg4everybody();
  scroll();
  slider();
  if ($("#map-canvas").length > 0) {
    map();
  }
  new Tabs();
});