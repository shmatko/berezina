import $ from "jquery";

global.$ = global.jQuery = $;
require("@fancyapps/fancybox");
require("social-likes");

export default () => {
  $(".social-likes").socialLikes();

  $(document).bind("mouseup touchend", e => {
    const touch = $(".touch-thing");
    if (!touch.is(e.target) && touch.has(e.target).length === 0) {
      $(touch).removeClass("active");
    }
  });

  $(".events-filter").click(function () {
    $(".events-filter").removeClass("active");
    $(this).toggleClass("active");
  });

  $(".scroll-top").click(() => {
    $("html, body").animate({
        scrollTop: 0
      },
      "slow"
    );
    return false;
  });

  $(document).on("click", ".smooth-scroll", function (event) {
    event.preventDefault();

    $("html, body").animate({
        scrollTop: $($.attr(this, "href")).offset().top
      },
      500
    );
  });

  const keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
  };

  function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault) e.preventDefault();
    e.returnValue = false;
  }

  function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      preventDefault(e);
      return false;
    }
  }

  function disableScroll() {
    if (window.addEventListener)
      window.addEventListener("DOMMouseScroll", preventDefault, false);
    window.onwheel = preventDefault;
    window.onmousewheel = document.onmousewheel = preventDefault;
    window.ontouchmove = preventDefault;
    document.onkeydown = preventDefaultForScrollKeys;
  }

  function enableScroll() {
    if (window.removeEventListener)
      window.removeEventListener("DOMMouseScroll", preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
  }

  setTimeout(() => {
    const form = document.querySelectorAll("form");
    if (form.length) {
      const inputs = $(form)
        .find(
          '[type="text"], [type="tel"], [type="email"], [type="search"], textarea'
        )
        .get();
      $(inputs).on("click focus change input", function (e) {
        const id = this.id;
        const label = document.querySelector(`[for="${id}"]`);
        if (!$(label).hasClass("active")) $(label).addClass("active");
      });
      $(inputs).on("blur", function (e) {
        const id = this.id;
        const label = document.querySelector(`[for="${id}"]`);
        if ($(this).val() === "") $(label).removeClass("active");
      });
    }
  }, 500);

  $(".callback-btn").click(() => {
    $(".popup--callback").addClass("active");
    disableScroll();
    if ($(window).width() < 768) {
      $("body").addClass("active");
    }
  });

  $(".order-btn").click(() => {
    $(".popup--order").addClass("active");
    disableScroll();
    if ($(window).width() < 768) {
      $("body").addClass("active");
    }
  });

  $(".popup-close").click(() => {
    $(".popup").removeClass("active");
    enableScroll();
    if ($(window).width() < 768) {
      $("body").removeClass("active");
    }
  });

  $(".search-btn").click(() => {
    $(".search__box").addClass("searching");
    $(".header").addClass("searching");
    $(".search-form__input").focus();
  });


  $(".search-close").click(() => {
    $(".search__box").removeClass("searching");
    $(".header").removeClass("searching");
  });

  $(document).bind("mouseup touchend", e => {
    const touch = $(".touch-thing");
    if (!touch.is(e.target) && touch.has(e.target).length === 0) {
      $(touch).removeClass("active");
    }
  });
  $(".menu-btn").click(() => {
    $(".menu").addClass("active");
    disableScroll();
  });

  $(".menu-close").click(() => {
    $(".menu").removeClass("active");
    enableScroll();
  });

  function scrollItems() {
    if ($(window).width() > 1279) {
      $(window).on("scroll", function () {

        if ($(this).scrollTop() >= 800) {
          $(".header--common").addClass("active");
        } else {
          $(".header--common").removeClass("active");
        }

        if ($(this).scrollTop() >= $(".first-section").outerHeight(true)) {
          $(".header").addClass("fixed");
          $(".sidebar").addClass("active");
        } else {
          $(".header").removeClass("fixed");
          $(".sidebar").removeClass("active");
        }
        if ($(this).scrollTop() >= $("body").outerHeight(true) - 1160) {
          $(".sidebar--consulting").addClass("bottom");
        } else {
          $(".sidebar--consulting").removeClass("bottom");
        }

        if ($(this).scrollTop() >= $(".content__sidebar").outerHeight(true)) {
          $(".sidebar--training").addClass("fade");
        } else {
          $(".sidebar--training").removeClass("fade");
        }

        if ($(this).scrollTop() >= $(".content__sidebar").outerHeight(true)) {
          $(".sidebar--pharm").addClass("stop");
        } else {
          $(".sidebar--pharm").removeClass("stop");
        }
      });
    }
  }
  scrollItems();
  $(window).resize(scrollItems);

  $(document).on("click", ".cases-slider__more", function () {
    disableScroll();
    const title = $(this)
      .siblings(".cases-slider__title")
      .html();
    const text = $(this)
      .siblings(".cases-slider__text")
      .html();
    $(".cases-box__title").html(title);
    $(".cases-box__text").html(text);
    $(".popup--cases").addClass("active");
  });

  $(".events-heading__icon").click(function () {
    $(".events-heading__icon").removeClass("active");
    $(this).addClass("active");
  });

  $(".events-heading__icon--bars").click(() => {
    $(".events-content__list").addClass("active");
  });

  $(".events-heading__icon--dots").click(() => {
    $(".events-content__list").removeClass("active");
  });

  const sidebarHeight = $(".consulting-section").height();
  const sidebarTop = $(".header").height();
  const position = $(".menu-btn").offset().left + 18;
  $(".sidebar").css({
    height: sidebarHeight,
    top: sidebarTop,
    left: position
  });

  $(".speakers-list__more").on("click", function () {

    $(".popup--speaker").addClass("active");
    const speakerImage = $(this).parents(".speakers-list__item").find('.speakers-list__image').attr("src");
    const speakerName = $(this).parents(".speakers-list__item").find('.speakers-list__title').text();
    const speakerPosition = $(this).parents(".speakers-list__item").find('.speakers-list__position').text();
    const speakerInfo = $(this).parents(".speakers-list__item").find('.speakers-list__text').html();
    $(".speaker-information__image").attr("src", speakerImage);
    $(".speaker-information__title").text(speakerName);
    $(".speaker-information__position").text(speakerPosition);
    $(".speaker-information__text").html(speakerInfo);
    $(".speaker-information__text").mCustomScrollbar();
  });
};