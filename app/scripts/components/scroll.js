import $ from "jquery";

require("malihu-custom-scrollbar-plugin");
require("jquery-mousewheel");

export default () => {
  $(".custom-scroll").mCustomScrollbar();

  if ($(window).width() < 768) {
    $(".menu-middle").mCustomScrollbar();
  }

  if ($(window).width() < 1740) {
    $(".calendar-slider").mCustomScrollbar({
      axis: "x",
      theme: "dark-3"
    });
  }

};