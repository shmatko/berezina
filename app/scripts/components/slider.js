import $ from "jquery";
import slick from "slick-carousel";

export default () => {
  $(".calendar-slider").slick({
    slidesToShow: 1,
    infinite: false,
    nextArrow: `
    <div class="slick-arrow calendar-slider__arrow calendar-slider__arrow--next">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-next"/></svg>
    </div>`,
    prevArrow: `
    <div class="slick-arrow calendar-slider__arrow calendar-slider__arrow--prev">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-prev"/></svg>
    </div>`,
    responsive: [{
      breakpoint: 1740,
      settings: "unslick"
    }]
  });

  $(".news-slider").slick({
    slidesToShow: 3,
    nextArrow: `
    <div class="slick-arrow news-slider__arrow slick-arrow__next">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-next"/></svg>
    </div>`,
    prevArrow: `
    <div class="slick-arrow news-slider__arrow slick-arrow__prev">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-prev"/></svg>
    </div>`,
    responsive: [{
        breakpoint: 1367,
        settings: {
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          arrows: false,
          dots: true
        }
      }
    ]
  });
  $(".reviews-slider").slick({
    slidesToShow: 1,
    nextArrow: `
    <div class="slick-arrow reviews-slider__arrow reviews-slider__arrow--next">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-next"/></svg>
    </div>`,
    prevArrow: `
    <div class="slick-arrow reviews-slider__arrow reviews-slider__arrow--prev">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-prev"/></svg>
    </div>`,
    dots: true
  });
  $(".training-slider").slick({
    slidesToShow: 1,
    fade: true,
    nextArrow: `
    <div class="slick-arrow training-slider__arrow training-slider__arrow--next">
    <svg class="slick-arrow__svg training-slider__svg"><use xlink:href="/images/sprite.svg#arrow-next"/></svg>
    </div>`,
    prevArrow: `
    <div class="slick-arrow training-slider__arrow training-slider__arrow--prev">
    <svg class="slick-arrow__svg training-slider__svg"><use xlink:href="/images/sprite.svg#arrow-prev"/></svg>
    </div>`,
    asNavFor: ".training-slider-small",
    responsive: [{
      breakpoint: 1279,
      settings: {
        arrows: false,
        dots: true
      }
    }]
  });
  $(".training-slider-small").slick({
    slidesToShow: 8,
    arrows: false,
    asNavFor: ".training-slider",
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1279,
      settings: "unslick"
    }]
  });
  $(".team-slider").slick({
    slidesToShow: 3,
    nextArrow: `
    <div class="slick-arrow team-slider__arrow slick-arrow__next">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-next"/></svg>
    </div>`,
    prevArrow: `
    <div class="slick-arrow team-slider__arrow slick-arrow__prev">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-prev"/></svg>
    </div>`,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          infinite: true,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          arrows: false,
          dots: true
        }
      }
    ]
  });
  $(".article-slider").slick({
    slidesToShow: 3,
    infinite: false,
    nextArrow: `
    <div class="slick-arrow team-slider__arrow slick-arrow__next">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-next"/></svg>
    </div>`,
    prevArrow: `
    <div class="slick-arrow team-slider__arrow slick-arrow__prev">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-prev"/></svg>
    </div>`,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          arrows: false,
          dots: true
        }
      }
    ]
  });
  $(".cases-slider").slick({
    slidesToShow: 3,
    nextArrow: `
    <div class="slick-arrow cases-slider__arrow slick-arrow__next">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-next"/></svg>
    </div>`,
    prevArrow: `
    <div class="slick-arrow cases-slider__arrow slick-arrow__prev">
    <svg class="slick-arrow__svg news-slider__svg"><use xlink:href="/images/sprite.svg#arrow-prev"/></svg>
    </div>`,
    responsive: [{
        breakpoint: 1281,
        settings: {
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          arrows: false,
          dots: true
        }
      }
    ]
  });
};